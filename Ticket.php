<?php


namespace App;


class Ticket
{
    private $number;
    private $title;
    private $last_name;

    /**
     * Ticket constructor.
     * @param $number
     * @param $title
     * @param $last_name
     */
    public function __construct($number, $title, $last_name)
    {
        $this->number = $number;
        $this->title = $title;
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }



}